<?php namespace Hampel\Twitter\Response;

/**
 * Represents a Hashtag entity
 *
 */
class Hashtag extends Entity
{
	/** @var string type of entity for matching purposes */
	protected $type = "hashtag";

}

?>