<?php namespace Hampel\Twitter\Response;

use Hampel\Json\Json;
use Hampel\Twitter\Service\TwitterException;

/**
 * Represents a user profile from Twitter
 *
 */
class User extends Response
{
	protected $url_entities;

	protected $description_entities;

	/**
	 * Sets data to the object
	 *
	 * @param array $data	data array from decoded JSON
	 */
	public function set(array $data)
	{
		if (!isset($data['id'])) throw new TwitterException("Invalid data received - no id found in user");
		if (!isset($data['id_str'])) throw new TwitterException("Invalid data received - no id_str found in user");

		$data['user_id'] = $data['id_str'];
		unset($data['id'], $data['id_str']);

		$data['created_at'] = strtotime($data['created_at']);

		if (isset($data['entities']))
		{
			if (isset($data['entities']['url']))
			{
				$this->url_entities = Entity::extractUserEntities($data['entities']['url'], $data['user_id']);
			}
			if (isset($data['entities']['description']))
			{
				$this->description_entities = Entity::extractUserEntities($data['entities']['description'], $data['user_id']);
			}
			unset($data['entities']);
		}

		// strip off some other object or array data we won't currently use
		if (isset($data['follow_request_sent'])) unset($data['follow_request_sent']);
		if (isset($data['following'])) unset($data['following']);
		if (isset($data['status'])) unset($data['status']);

		parent::set($data);
	}

	/**
	 * Get the Twitter user_id of this user
	 *
	 * @return string user id
	 */
	public function getUserId()
	{
		return $this->data['user_id'];
	}

	/**
	 * Get the Twitter screen_name of this user
	 *
	 * @return string screen_name
	 */
	public function getScreenName()
	{
		return $this->data['screen_name'];
	}

	/**
	 * Build an array of User objects returned by a Twitter API call
	 *
	 * @param string $json	JSON data returned by Twitter
	 *
	 * @return array of User objects
	 */
	public static function extractUsers($json)
	{
		$user_array = array();

		if (empty($json)) return $user_array;

		$data = Json::decode($json, true);

		foreach ($data as $user_info)
		{
			$user = new User();
			$user->set($user_info);
			$user_array[] = $user;
		}

		return $user_array;
	}

	/**
	 * Build an single User object returned by a Twitter API call
	 *
	 * @param string $json	JSON data returned by Twitter
	 *
	 * @return User objects
	 */
	public static function extractUser($json)
	{
		if (empty($json)) return null;

		$data = Json::decode($json, true);

		$user = new User();
		$user->set($data);

		return $user;
	}
}

?>