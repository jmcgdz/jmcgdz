<?php namespace Hampel\Twitter\Response;

use Hampel\Twitter\Service\TwitterException;

/**
 * Represents a User Mention entity
 *
 */
class UserMention extends Entity
{
	/** @var string type of entity for matching purposes */
	protected $type = "usermention";

	/**
	 * Sets data to the object
	 *
	 * @param array $data			data array from decoded JSON
	 */
	public function set(array $data)
	{
		if (!isset($data['id'])) throw new TwitterException("Invalid data received - no id found in user mention");
		if (!isset($data['id_str'])) throw new TwitterException("Invalid data received - no id_str found in user mention");

		$data['user_id'] = $data['id_str'];
		unset($data['id'], $data['id_str']);

		parent::set($data);
	}
}

?>