<?php namespace Hampel\Twitter\Response;

/**
 * Represents a URL entity
 *
 */
class Url extends Entity
{
	/** @var string type of entity for matching purposes */
	protected $type = "url";

}

?>