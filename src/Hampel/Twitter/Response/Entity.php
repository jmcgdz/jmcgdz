<?php namespace Hampel\Twitter\Response;

use Hampel\Twitter\Service\TwitterException;

/**
 * Parent class for entity responses - provides common functionality
 *
 */
abstract class Entity extends Response
{
	/** @var string Type of entity child object refers to - used to match against JSON data from Twitter */
	protected $type;

	/**
	 * Sets data to the object
	 *
	 * @param array $data			data array from decoded JSON
	 */
	public function set(array $data)
	{
		$data['type'] = $this->type;

		if (!isset($data['indices'][0])) throw new TwitterException("Invalid data received - no start index found");
		if (!isset($data['indices'][1])) throw new TwitterException("Invalid data received - no end index found");

		$data['start'] = $data['indices'][0];
		$data['end'] = $data['indices'][1];

		unset($data['indices']);

		parent::set($data);
	}

	/**
	 * Build an array of Entity objects returned by a Twitter API call
	 *
	 * @param array $entities 	Array of entity arrays returned from decoded JSON data
	 * @param string $status_id	status_id of the status this entity belongs to
	 *
	 * @throws TwitterException
	 *
	 * @return array of either Hashtag, Url, UserMention or Media objects
	 */
	public static function extractEntities(array $entities, $status_id)
	{
		$entity_array = array();

		if (empty($entities)) return $entity_array;

		foreach ($entities as $type => $type_list)
		{
			foreach ($type_list as $data)
			{
				if ($type == "hashtags") $entity = new Hashtag();
				elseif ($type == "urls") $entity = new Url();
				elseif ($type == "user_mentions") $entity = new UserMention();
				elseif ($type == "media") $entity = new Media();
				else throw new TwitterException("Unknown entity type: " . $type, $status_id);

				$data['status_id'] = $status_id;
				$entity->set($data);
				$entity_array[] = $entity;
			}
		}

		return $entity_array;
	}

	/**
	 * Build an array of Entity objects returned by a Twitter API call
	 *
	 * @param array $entities 	Array of entity arrays returned from decoded JSON data
	 * @param string $user_id	user_id of the user this entity belongs to
	 *
	 * @throws TwitterException
	 *
	 * @return array of Url objects
	 */
	public static function extractUserEntities(array $entities, $user_id)
	{
		$entity_array = array();

		if (empty($entities)) return $entity_array;

		foreach ($entities as $type => $type_list)
		{
			foreach ($type_list as $data)
			{
				if ($type == "urls") $entity = new Url();
				else throw new TwitterException("Unknown entity type: " . $type, $user_id);

				$data['user_id'] = $user_id;
				$entity->set($data);
				$entity_array[] = $entity;
			}
		}

		return $entity_array;
	}


}

?>