<?php namespace Hampel\Twitter\Response;

use Carbon\Carbon;
use Hampel\Json\Json;

/**
 * Represents a rate limit object
 *
 */
class RateLimit extends Response
{
	/**
	 * Build an array of RateLimit objects representing the resources returned by the Twitter application/rate_limit_status API call
	 *
	 * @param string $json	JSON data returned by Twitter
	 *
	 * @return array of RateLimit objects
	 */
	public static function extractAllRateLimits($json)
	{
		$resource_array = array();

		if (empty($json)) return $resource_array;

		$data = Json::decode($json, true);

		foreach ($data['resources'] as $key => $value)
		{
			foreach ($value as $name => $ratelimit_array)
			{
				$ratelimit = new RateLimit();
				$ratelimit_array['name'] = $name;
				$ratelimit->set($ratelimit_array);
				$resource_array[$name] = $ratelimit;
			}
		}

		return $resource_array;
	}

	/**
	 * Extract a single RateLimit object from the resources returned by the Twitter application/rate_limit_status API call
	 *
	 * @param string $json 		JSON data returned by Twitter
	 * @param string $resource	name of the resource to return data for
	 *
	 * @return RateLimit object or NULL if resource not found
	 */
	public static function extractSingleRateLimit($json, $resource)
	{
		if (empty($json) OR empty($resource)) return null;

		$data = Json::decode($json, true);

		foreach ($data['resources'] as $key => $value)
		{
			foreach ($value as $name => $ratelimit_array)
			{
				if ($name == $resource)
				{
					$ratelimit = new RateLimit();
					$ratelimit_array['name'] = $name;
					$ratelimit->set($ratelimit_array);
					return $ratelimit;
				}
			}
		}

		return null;
	}

	/**
	 * Get remaining hits for this API call
	 *
	 * @return number rate limit hits remaining
	 */
	public function getRemaining()
	{
		if (array_key_exists('remaining', $this->data)) return $this->data['remaining'];
		else return false;
	}

	/**
	 * Get name of this rate limit
	 *
	 * @return string name of rate limit
	 */
	public function getName()
	{
		if (array_key_exists('name', $this->data)) return $this->data['name'];
		else return "";

	}

	/**
	 * Get a string representation of this rate limit
	 *
	 * @return string rate limit info
	 */
	public function toString()
	{
		if (!isset($this->data)) return "";
		if (!isset($this->data['name'])) return "";

		$difference = Carbon::createFromTimestampUTC($this->data['reset'])->diffForHumans();

		return "{$this->data['name']} has {$this->data['remaining']} of {$this->data['limit']} hits remaining and resets in {$difference}";
	}
}

?>