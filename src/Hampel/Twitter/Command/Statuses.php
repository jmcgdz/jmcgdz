<?php namespace Hampel\Twitter\Command;

use Hampel\Twitter\Response\Status;
use Hampel\Twitter\Service\TwitterException;

/**
 * Statuses Twitter API group
 *
 */
class Statuses extends Family
{
	/** @var string Prefix for commands */
	protected $prefix = 'statuses';

	/**
	 * Wrapper for statuses/user_timeline API call
	 *
	 * @param string $user_id		Twitter user_id to get timeline for. Use this rather than screen_name if available.
	 * @param string $screen_name	Twitter screen_name to get timeline for. If both user_id and screen_name supplied, user_id will be used.
	 * @param string $since_id		Return only tweets since (and not including) this Twitter status_id
	 * @param string $max_id		Return tweets up to (and including) this Twitter status_id
	 * @param number $count			Return this many tweets (maximum 200)
	 *
	 * @throws TwitterException
	 *
	 * @return mixed array of Status objects
	 */
	public function getUserTimeline($user_id, $screen_name = "", $since_id = "", $max_id = "", $count = 0)
	{
		$command = $this->prefix . '/user_timeline';

		$rateLimited = $this->isRateLimited($command);
		if ($rateLimited !== false) throw new TwitterException("About to get rate limited - " . $rateLimited->toString());

		$headers = array();
		$options = array('query' => array());

		if (!empty($user_id)) $options['query']['user_id'] = $user_id;
		else $options['query']['screen_name'] = $screen_name;

		if (!empty($since_id)) $options['query']['since_id'] = $since_id;
		if (!empty($max_id)) $options['query']['max_id'] = $max_id;
		if ($count > 0) $options['query']['count'] = $count;

		$response = $this->twitter->get($command, $headers, $options);

		$response_json = $response->getBody(true);

		if (empty($response_json)) throw new TwitterException("Empty body received from {$command}");

		return Status::extractStatuses($response_json, $user_id);
	}

	/**
	 * Get Earliest Status ID
	 * Cursors back through user timeline to locate the earliest status id for the given user
	 * Operates recursively - set start_id to a blank string or do not specify start_id when calling
	 *
	 * @param string $user_id		Twitter user_id to get timeline for. Use this rather than screen_name if available.
	 * @param string $screen_name	Twitter screen_name to get timeline for. If both user_id and screen_name supplied, user_id will be used.
	 * @param string $start_id		Return tweets up to (and including) this Twitter status_id (only used for recursion)
	 *
	 * @return string earliest status_id found
	 */
	public function getEarliestStatusId($user_id, $screen_name = "", $start_id = "")
	{
		if (!empty($start_id)) $max_id = bcsub($start_id, '1');
		else $max_id = "";

		$statuses = $this->getUserTimeline($user_id, $screen_name, "", $max_id, 200);
		if (empty($statuses)) return $start_id; // stop now

		foreach ($statuses as $status)
		{
			$status_id = $status->getStatusId();
			if (empty($start_id) OR $status_id < $start_id) $start_id = $status_id;
		}

		return $this->getEarliestStatusId($user_id, $screen_name, $start_id);
	}

	/**
	 * Gets all statuses posted by a user
	 * Cursors back through user timeline collecting statuses for the given particular user
	 * Operates recursively - set start_id to a blank string or do not specify start_id when calling
	 *
	 * @param string $user_id		Twitter user_id to get timeline for. Use this rather than screen_name if available.
	 * @param string $screen_name	Twitter screen_name to get timeline for. If both user_id and screen_name supplied, user_id will be used.
	 * @param string $start_id		Return tweets up to (and including) this Twitter status_id (only used for recursion)
	 *
	 * @return mixed array of Status objects
	 */
	public function getAllStatuses($user_id, $screen_name = "", $start_id = "")
	{
		if (!empty($start_id)) $max_id = bcsub($start_id, '1');
		else $max_id = "";

		$statuses = $this->getUserTimeline($user_id, $screen_name, "", $max_id, 200);
		if (empty($statuses)) return array(); // stop now

		foreach ($statuses as $status)
		{
			$status_id = $status->getStatusId();
			if (empty($start_id) OR $status_id < $start_id) $start_id = $status_id;
		}

		return $statuses + $this->getAllStatuses($user_id, $screen_name, $start_id);
	}

}

?>