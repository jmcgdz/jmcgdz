<?php namespace Hampel\Twitter\Service;

/**
 * Provides configuration information for the Twitter service
 *
 * You might want to create your own subclass to provide additional functionality for things like reading config from a database or from the file system
 *
 */
class TwitterConfig
{
	/** @var string Oauth consumer key as provided by Twitter */
	protected $consumer_key;

	/** @var string Oauth consumer secret as provided by Twitter */
	protected $consumer_secret;

	/** @var string Oauth token as provided by Twitter */
	protected $token;

	/** @var string Oauth token secret as provided by Twitter */
	protected $token_secret;

	/** @var number minimum rate limit remaining before we should consider ourselves "about to be rate limited" (not currently used) */
	protected $min_rate_limit;

	/**
	 * Set configuration
	 *
	 * @param string $consumer_key
	 * @param string $consumer_secret
	 * @param string $token
	 * @param string $token_secret
	 * @param number $min_rate_limit
	 */
	public function set($consumer_key, $consumer_secret, $token, $token_secret, $min_rate_limit = 5)
	{
		$this->consumer_key = $consumer_key;
		$this->consumer_secret = $consumer_secret;
		$this->token = $token;
		$this->token_secret = $token_secret;

		$this->min_rate_limit = $min_rate_limit;
	}

	/**
	 * Get an array of Oauth parameters suitable for passing to Guzzle's OauthPlugin constructor
	 *
	 * @return array of configuration data
	 */
	public function getOauthParamArray()
	{
		return array(
			'consumer_key' => $this->consumer_key,
			'consumer_secret' => $this->consumer_secret,
			'token' => $this->token,
			'token_secret' => $this->token_secret
		);
	}

	/**
	 * Get the oauth consumer key
	 *
	 * @return string
	 */
	public function getConsumerKey()
	{
		return $this->consumer_key;
	}

	/**
	 * Get the consumer secret
	 *
	 * @return string
	 */
	public function getConsumerSecret()
	{
		return $this->consumer_secret;
	}

	/**
	 * Get the oauth token
	 *
	 * @return string
	 */
	public function getToken()
	{
		return $this->token;
	}

	/**
	 * Get the oauth token secret
	 *
	 * @return string
	 */
	public function getTokenSecret()
	{
		return $this->token_secret;
	}

	/**
	 * Get the minimum rate limit
	 *
	 * @return number
	 */
	public function getMinRateLimit()
	{
		return $this->min_rate_limit;
	}
}

?>