<?php namespace Hampel\Twitter\Command;

use Hampel\Twitter\Service\TwitterService;
use Hampel\Twitter\Command\Users;
use Hampel\Twitter\Response\User;
use Guzzle\Service\Client;
use Guzzle\Tests\GuzzleTestCase;

class UsersTest extends GuzzleTestCase
{
	public function setUp()
	{
		date_default_timezone_set('UTC');

		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testGetShowUser()
	{
		$client = new Client();
		$this->setMockResponse($client, array('rate_limit_users.json', 'user_show_twitterapi.json'));

		$twitter = new TwitterService($client);
		$twitter->init();
		$users = new Users($twitter);
		$data = $users->getShowUser(0, "twitterapi");

		$this->assertTrue($data instanceof User);
		$this->assertEquals('6253282', $data->getUserId());
		$this->assertEquals('twitterapi', $data->getScreenName());
	}

	public function testPostLookupUsersByUserId()
	{
		$client = new Client();
		$this->setMockResponse($client, array('rate_limit_users.json', 'user_lookup_by_userid.json'));

		$twitter = new TwitterService($client);
		$twitter->init();
		$users = new Users($twitter);
		$data = $users->postLookupUsersByUserId(array('6253282', '783214'));

		$this->assertTrue(is_array($data));
		$this->assertArrayHasKey(0, $data);
		$this->assertTrue($data[0] instanceof User);
		$this->assertEquals('twitter', $data[0]->getScreenName());
		$this->assertArrayHasKey(1, $data);
		$this->assertTrue($data[1] instanceof User);
		$this->assertEquals('twitterapi', $data[1]->getScreenName());
	}

	public function testPostLookupUsersByScreenName()
	{
		$client = new Client();
		$this->setMockResponse($client, array('rate_limit_users.json', 'user_lookup_by_userid.json'));

		$twitter = new TwitterService($client);
		$twitter->init();
		$users = new Users($twitter);
		$data = $users->postLookupUsersByScreenName(array('twitterapi', 'twitter'));

		$this->assertTrue(is_array($data));
		$this->assertArrayHasKey(0, $data);
		$this->assertTrue($data[0] instanceof User);
		$this->assertEquals('twitter', $data[0]->getScreenName());
		$this->assertArrayHasKey(1, $data);
		$this->assertTrue($data[1] instanceof User);
		$this->assertEquals('twitterapi', $data[1]->getScreenName());
	}

}

?>