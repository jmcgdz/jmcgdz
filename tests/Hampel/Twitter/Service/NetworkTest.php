<?php namespace Hampel\Twitter\Service;

use Guzzle\Http\Client;
use Guzzle\Plugin\Oauth\OauthPlugin;
use Hampel\Twitter\Command\Application;
use Hampel\Twitter\Command\Users;
use Hampel\Twitter\Command\Statuses;
use Hampel\Twitter\Response\RateLimit;
use Hampel\Twitter\Response\User;

class NetworkTest extends \PHPUnit_Framework_TestCase
{
	protected $twitter;

	public function setUp()
	{
		date_default_timezone_set('UTC');

		$config = new TwitterConfig();
		$config->set(CONSUMER_KEY, CONSUMER_SECRET, TOKEN, TOKEN_SECRET);
		$oauth = new OauthPlugin($config->getOauthParamArray());
		$client = new Client();
		$twitter = new TwitterService($client, $oauth);
		$twitter->init();
		$this->twitter = $twitter;
	}

	/**
	 * @group network
	 */
	public function testRateLimit()
	{
		$application = new Application($this->twitter);
		$data = $application->getRateLimit('application');
		$status_code = $this->twitter->getLastStatusCode();

		$this->assertEquals(200, $status_code);
		$this->assertArrayHasKey('/application/rate_limit_status', $data);
		$this->assertTrue($data['/application/rate_limit_status'] instanceof RateLimit);
		$this->assertEquals('/application/rate_limit_status', $data['/application/rate_limit_status']->getName());
	}

	/**
	 * @group network
	 */
	public function testGetShowUser()
	{
		$users = new Users($this->twitter);
		$data = $users->getShowUser(0, "twitterapi");
		$status_code = $this->twitter->getLastStatusCode();

		$this->assertEquals(200, $status_code);
		$this->assertTrue($data instanceof User);
	}

	/**
	 * @group network
	 */
	public function testPostLookupUsersByUserId()
	{
		$users = new Users($this->twitter);
		$data = $users->postLookupUsersByUserId(array('6253282', '783214'));
		$status_code = $this->twitter->getLastStatusCode();

		$this->assertEquals(200, $status_code);
		$this->assertTrue(is_array($data));
		$this->assertArrayHasKey(0, $data);
		$this->assertTrue($data[0] instanceof User);
	}

	/**
	 * @group network
	 */
	public function testPostLookupUsersByScreenName()
	{
		$users = new Users($this->twitter);
		$data = $users->postLookupUsersByScreenName(array('twitterapi', 'twitter'));
		$status_code = $this->twitter->getLastStatusCode();

		$this->assertEquals(200, $status_code);
		$this->assertTrue(is_array($data));
		$this->assertArrayHasKey(0, $data);
		$this->assertTrue($data[0] instanceof User);
	}

	/**
	 * @group network
	 */
	public function testGetEarliestStatusId()
	{
		$statuses = new Statuses($this->twitter);
		$data = $statuses->getEarliestStatusId(0, "twitterapi");
		$status_code = $this->twitter->getLastStatusCode();

		$this->assertEquals(200, $status_code);
		$this->assertTrue(is_string($data));
	}

	/**
	 * @group network
	 */
	public function testGetAllStatuses()
	{
		$statuses = new Statuses($this->twitter);
		$data = $statuses->getAllStatuses(0, "twitterapi");
		$status_code = $this->twitter->getLastStatusCode();

		$this->assertEquals(200, $status_code);
		$this->assertTrue(is_array($data));
	}
}

?>