<?php namespace Hampel\Twitter\Response;

use Carbon\Carbon;
use Hampel\Json\Json;

class RateLimitTest extends \PHPUnit_Framework_TestCase
{
	public $ratelimit;

	public function setUp()
	{
		$this->ratelimit = new RateLimit();
		date_default_timezone_set('UTC');
	}

	public function testToStringWhenNotSet()
	{
		$this->assertSame("", $this->ratelimit->toString());
	}

	public function testSet()
	{
		$datajson = '{
			"remaining": 12,
			"reset": 1346439527,
			"limit": 15
		}';

		$data = json_decode($datajson, true);
		$data['name'] = '/help/privacy';

		$this->assertFalse(is_null($data));
		$this->assertTrue(is_array($data));

		$this->ratelimit->set($data);

		$info = $this->ratelimit->get();

		$this->assertSame('/help/privacy', $info['name']);
		$this->assertSame(12, $info['remaining']);
		$this->assertSame(1346439527, $info['reset']);
		$this->assertSame(15, $info['limit']);
	}

	public function testToString()
	{
		$fivemins = Carbon::now()->addMinutes(5)->getTimestamp();

		$datajson = '{
			"remaining": 12,
			"reset": ' . $fivemins . ',
			"limit": 15
		}';

		$data = Json::decode($datajson, true);

		$data['name'] = '/help/privacy';

		$this->ratelimit->set($data);
		$this->assertSame("/help/privacy has 12 of 15 hits remaining and resets in 5 minutes from now", $this->ratelimit->toString());
	}

	public function testExtractAllRateLimits()
	{
		$json = '{"rate_limit_context":{"access_token":"173423638-CeKgF1dQvvDC8qJ9OOFcpjzUhpS2UnozECqa02s6"},"resources":{"lists":{"/lists/subscribers":{"limit":180,"remaining":180,"reset":1371187137},"/lists/memberships":{"limit":15,"remaining":15,"reset":1371187137},"/lists/list":{"limit":15,"remaining":15,"reset":1371187137},"/lists/ownerships":{"limit":15,"remaining":15,"reset":1371187137},"/lists/subscriptions":{"limit":15,"remaining":15,"reset":1371187137},"/lists/members":{"limit":180,"remaining":180,"reset":1371187137},"/lists/subscribers/show":{"limit":15,"remaining":15,"reset":1371187137},"/lists/statuses":{"limit":180,"remaining":180,"reset":1371187137},"/lists/show":{"limit":15,"remaining":15,"reset":1371187137},"/lists/members/show":{"limit":15,"remaining":15,"reset":1371187137}},"application":{"/application/rate_limit_status":{"limit":180,"remaining":179,"reset":1371187137}},"friendships":{"/friendships/incoming":{"limit":15,"remaining":15,"reset":1371187137},"/friendships/lookup":{"limit":15,"remaining":15,"reset":1371187137},"/friendships/outgoing":{"limit":15,"remaining":15,"reset":1371187137},"/friendships/no_retweets/ids":{"limit":15,"remaining":15,"reset":1371187137},"/friendships/show":{"limit":180,"remaining":180,"reset":1371187137}},"blocks":{"/blocks/ids":{"limit":15,"remaining":15,"reset":1371187137},"/blocks/list":{"limit":15,"remaining":15,"reset":1371187137}},"geo":{"/geo/similar_places":{"limit":15,"remaining":15,"reset":1371187137},"/geo/search":{"limit":15,"remaining":15,"reset":1371187137},"/geo/reverse_geocode":{"limit":15,"remaining":15,"reset":1371187137},"/geo/id/:place_id":{"limit":15,"remaining":15,"reset":1371187137}},"users":{"/users/profile_banner":{"limit":180,"remaining":180,"reset":1371187137},"/users/suggestions/:slug/members":{"limit":15,"remaining":15,"reset":1371187137},"/users/show/:id":{"limit":180,"remaining":180,"reset":1371187137},"/users/suggestions":{"limit":15,"remaining":15,"reset":1371187137},"/users/lookup":{"limit":180,"remaining":180,"reset":1371187137},"/users/search":{"limit":180,"remaining":180,"reset":1371187137},"/users/contributors":{"limit":15,"remaining":15,"reset":1371187137},"/users/contributees":{"limit":15,"remaining":15,"reset":1371187137},"/users/suggestions/:slug":{"limit":15,"remaining":15,"reset":1371187137}},"followers":{"/followers/list":{"limit":15,"remaining":15,"reset":1371187137},"/followers/ids":{"limit":15,"remaining":15,"reset":1371187137}},"statuses":{"/statuses/mentions_timeline":{"limit":15,"remaining":15,"reset":1371187137},"/statuses/show/:id":{"limit":180,"remaining":180,"reset":1371187137},"/statuses/oembed":{"limit":180,"remaining":180,"reset":1371187137},"/statuses/home_timeline":{"limit":15,"remaining":15,"reset":1371187137},"/statuses/retweeters/ids":{"limit":15,"remaining":15,"reset":1371187137},"/statuses/user_timeline":{"limit":180,"remaining":180,"reset":1371187137},"/statuses/retweets_of_me":{"limit":15,"remaining":15,"reset":1371187137},"/statuses/retweets/:id":{"limit":15,"remaining":15,"reset":1371187137}},"help":{"/help/privacy":{"limit":15,"remaining":15,"reset":1371187137},"/help/tos":{"limit":15,"remaining":15,"reset":1371187137},"/help/configuration":{"limit":15,"remaining":15,"reset":1371187137},"/help/languages":{"limit":15,"remaining":15,"reset":1371187137}},"friends":{"/friends/ids":{"limit":15,"remaining":15,"reset":1371187137},"/friends/list":{"limit":15,"remaining":15,"reset":1371187137}},"direct_messages":{"/direct_messages/show":{"limit":15,"remaining":15,"reset":1371187137},"/direct_messages/sent_and_received":{"limit":15,"remaining":15,"reset":1371187137},"/direct_messages/sent":{"limit":15,"remaining":15,"reset":1371187137},"/direct_messages":{"limit":15,"remaining":15,"reset":1371187137}},"account":{"/account/verify_credentials":{"limit":15,"remaining":15,"reset":1371187137},"/account/settings":{"limit":15,"remaining":15,"reset":1371187137}},"favorites":{"/favorites/list":{"limit":15,"remaining":15,"reset":1371187137}},"saved_searches":{"/saved_searches/destroy/:id":{"limit":15,"remaining":15,"reset":1371187137},"/saved_searches/list":{"limit":15,"remaining":15,"reset":1371187137},"/saved_searches/show/:id":{"limit":15,"remaining":15,"reset":1371187137}},"search":{"/search/tweets":{"limit":180,"remaining":180,"reset":1371187137}},"trends":{"/trends/available":{"limit":15,"remaining":15,"reset":1371187137},"/trends/place":{"limit":15,"remaining":15,"reset":1371187137},"/trends/closest":{"limit":15,"remaining":15,"reset":1371187137}}}}';

		$data = RateLimit::extractAllRateLimits($json);

		$this->assertArrayHasKey('/lists/subscribers', $data);

		$info = $data['/lists/subscribers']->get();

		$this->assertEquals('/lists/subscribers', $info['name']);
		$this->assertEquals(180, $info['limit']);
		$this->assertEquals(180, $info['remaining']);
		$this->assertEquals(1371187137, $info['reset']);
	}

	public function testExtractSingleRateLimit()
	{
		$json = '{"rate_limit_context":{"access_token":"173423638-CeKgF1dQvvDC8qJ9OOFcpjzUhpS2UnozECqa02s6"},"resources":{"lists":{"/lists/subscribers":{"limit":180,"remaining":180,"reset":1371187137},"/lists/memberships":{"limit":15,"remaining":15,"reset":1371187137},"/lists/list":{"limit":15,"remaining":15,"reset":1371187137},"/lists/ownerships":{"limit":15,"remaining":15,"reset":1371187137},"/lists/subscriptions":{"limit":15,"remaining":15,"reset":1371187137},"/lists/members":{"limit":180,"remaining":180,"reset":1371187137},"/lists/subscribers/show":{"limit":15,"remaining":15,"reset":1371187137},"/lists/statuses":{"limit":180,"remaining":180,"reset":1371187137},"/lists/show":{"limit":15,"remaining":15,"reset":1371187137},"/lists/members/show":{"limit":15,"remaining":15,"reset":1371187137}},"application":{"/application/rate_limit_status":{"limit":180,"remaining":179,"reset":1371187137}},"friendships":{"/friendships/incoming":{"limit":15,"remaining":15,"reset":1371187137},"/friendships/lookup":{"limit":15,"remaining":15,"reset":1371187137},"/friendships/outgoing":{"limit":15,"remaining":15,"reset":1371187137},"/friendships/no_retweets/ids":{"limit":15,"remaining":15,"reset":1371187137},"/friendships/show":{"limit":180,"remaining":180,"reset":1371187137}},"blocks":{"/blocks/ids":{"limit":15,"remaining":15,"reset":1371187137},"/blocks/list":{"limit":15,"remaining":15,"reset":1371187137}},"geo":{"/geo/similar_places":{"limit":15,"remaining":15,"reset":1371187137},"/geo/search":{"limit":15,"remaining":15,"reset":1371187137},"/geo/reverse_geocode":{"limit":15,"remaining":15,"reset":1371187137},"/geo/id/:place_id":{"limit":15,"remaining":15,"reset":1371187137}},"users":{"/users/profile_banner":{"limit":180,"remaining":180,"reset":1371187137},"/users/suggestions/:slug/members":{"limit":15,"remaining":15,"reset":1371187137},"/users/show/:id":{"limit":180,"remaining":180,"reset":1371187137},"/users/suggestions":{"limit":15,"remaining":15,"reset":1371187137},"/users/lookup":{"limit":180,"remaining":180,"reset":1371187137},"/users/search":{"limit":180,"remaining":180,"reset":1371187137},"/users/contributors":{"limit":15,"remaining":15,"reset":1371187137},"/users/contributees":{"limit":15,"remaining":15,"reset":1371187137},"/users/suggestions/:slug":{"limit":15,"remaining":15,"reset":1371187137}},"followers":{"/followers/list":{"limit":15,"remaining":15,"reset":1371187137},"/followers/ids":{"limit":15,"remaining":15,"reset":1371187137}},"statuses":{"/statuses/mentions_timeline":{"limit":15,"remaining":15,"reset":1371187137},"/statuses/show/:id":{"limit":180,"remaining":180,"reset":1371187137},"/statuses/oembed":{"limit":180,"remaining":180,"reset":1371187137},"/statuses/home_timeline":{"limit":15,"remaining":15,"reset":1371187137},"/statuses/retweeters/ids":{"limit":15,"remaining":15,"reset":1371187137},"/statuses/user_timeline":{"limit":180,"remaining":180,"reset":1371187137},"/statuses/retweets_of_me":{"limit":15,"remaining":15,"reset":1371187137},"/statuses/retweets/:id":{"limit":15,"remaining":15,"reset":1371187137}},"help":{"/help/privacy":{"limit":15,"remaining":15,"reset":1371187137},"/help/tos":{"limit":15,"remaining":15,"reset":1371187137},"/help/configuration":{"limit":15,"remaining":15,"reset":1371187137},"/help/languages":{"limit":15,"remaining":15,"reset":1371187137}},"friends":{"/friends/ids":{"limit":15,"remaining":15,"reset":1371187137},"/friends/list":{"limit":15,"remaining":15,"reset":1371187137}},"direct_messages":{"/direct_messages/show":{"limit":15,"remaining":15,"reset":1371187137},"/direct_messages/sent_and_received":{"limit":15,"remaining":15,"reset":1371187137},"/direct_messages/sent":{"limit":15,"remaining":15,"reset":1371187137},"/direct_messages":{"limit":15,"remaining":15,"reset":1371187137}},"account":{"/account/verify_credentials":{"limit":15,"remaining":15,"reset":1371187137},"/account/settings":{"limit":15,"remaining":15,"reset":1371187137}},"favorites":{"/favorites/list":{"limit":15,"remaining":15,"reset":1371187137}},"saved_searches":{"/saved_searches/destroy/:id":{"limit":15,"remaining":15,"reset":1371187137},"/saved_searches/list":{"limit":15,"remaining":15,"reset":1371187137},"/saved_searches/show/:id":{"limit":15,"remaining":15,"reset":1371187137}},"search":{"/search/tweets":{"limit":180,"remaining":180,"reset":1371187137}},"trends":{"/trends/available":{"limit":15,"remaining":15,"reset":1371187137},"/trends/place":{"limit":15,"remaining":15,"reset":1371187137},"/trends/closest":{"limit":15,"remaining":15,"reset":1371187137}}}}';

		$data = RateLimit::extractSingleRateLimit($json, '/lists/subscribers');

		$info = $data->get();

		$this->assertEquals('/lists/subscribers', $info['name']);
		$this->assertEquals(180, $info['limit']);
		$this->assertEquals(180, $info['remaining']);
		$this->assertEquals(1371187137, $info['reset']);

		$data = RateLimit::extractSingleRateLimit('', '/lists/subscribers');
		$this->assertNull($data);

		$data = RateLimit::extractSingleRateLimit($json, '');
		$this->assertNull($data);

		$data = RateLimit::extractSingleRateLimit($json, 'blah');
		$this->assertNull($data);
	}
}

?>